package com.intact.tasch.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.intact.tasch.R;
import com.intact.tasch.adapter.ProductCatalogAdapter;
import com.intact.tasch.adapter.WishListAdapter;
import com.intact.tasch.client.DataRequest;
import com.intact.tasch.client.OfflineDataHandler;
import com.intact.tasch.model.DataModel;
import com.intact.tasch.model.Product;
import com.intact.tasch.utility.Constants;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends BaseActivity implements ProductCatalogAdapter.ClickListener, WishListAdapter.WishListClickListener, View.OnClickListener {

    private TextView mEmptyWishList;
    private RecyclerView mProductCatalogRecyclerView;
    private ProductCatalogAdapter mProductCatalogAdapter;
    List<Product> mCatalogProductList;
    private List<Product> mWishListProducts = new ArrayList<>();
    private RecyclerView mWishListRecyclerView;
    private WishListAdapter mWishListAdapter;
    private TextView mTotal;
    private TextView mSubtotal;
    private Button mCheckout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
        setListeners();
        fetchProducts();
        setProductCatalogAdapter();
        setWishListAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWishListProducts.size() > 0) {
            mEmptyWishList.setVisibility(View.GONE);
            mWishListRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mEmptyWishList.setVisibility(View.VISIBLE);
            mWishListRecyclerView.setVisibility(View.INVISIBLE);
        }
    }

    private void initViews() {
        mToolbar = findViewById(R.id.toolbar_actionbar);
        mEmptyWishList = findViewById(R.id.empty_wish_list);
        mProductCatalogRecyclerView = findViewById(R.id.product_catalog_recycler_view);
        final LinearLayoutManager catalogLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        mProductCatalogRecyclerView.setLayoutManager(catalogLayoutManager);
        mProductCatalogRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mWishListRecyclerView = findViewById(R.id.wish_list_product_recycler_view);
        final LinearLayoutManager wishListLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mWishListRecyclerView.setLayoutManager(wishListLayoutManager);
        mWishListRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mTotal = findViewById(R.id.total);
        mSubtotal = findViewById(R.id.subtotal_price_text);
        mCheckout = findViewById(R.id.check_out);
    }

    private void setListeners() {
        mCheckout.setOnClickListener(this);
    }

    private void fetchProducts() {
        final DataRequest request = new DataRequest();
        request.setFilePath(Constants.FILE_PATH);
        request.setDataModel(new DataModel());
        request.setContext(this);
        OfflineDataHandler dataHandler = new OfflineDataHandler();
        dataHandler.fetchAssetsData(request, new OfflineDataHandler.ResponseListener() {
            @Override
            public void onResponse() {
                handleData(request);
            }
        });
    }

    private void handleData(DataRequest request) {
        mCatalogProductList = request.getDataModel().getProduct();
        updateCatalogAdapterData();
    }

    private void setProductCatalogAdapter() {
        mProductCatalogAdapter = new ProductCatalogAdapter(this, this);
        mProductCatalogRecyclerView.setAdapter(mProductCatalogAdapter);
    }

    @Override
    public void onItemClick(Product product) {
        startProductDetailsActivity(product);
    }

    private void startProductDetailsActivity(Product product) {
        startActivityForResult(ProductDetailsActivity.getMapsActivityIntent(this, product), Constants.REQUEST_CODE);
    }

    private void updateCatalogAdapterData() {
        mProductCatalogAdapter.setDataList(mCatalogProductList);
    }

    private void setWishListAdapter() {
        mWishListAdapter = new WishListAdapter(this, this);
        mWishListRecyclerView.setAdapter(mWishListAdapter);
    }

    private void updateWishListAdapterData() {
        mWishListAdapter.setDataList(mWishListProducts);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == Constants.REQUEST_CODE) {
            if (intent != null && intent.getExtras() != null) {
                if (intent.hasExtra(Constants.TAG_PRODUCT_MODEL)) {
                    Product product = intent.getExtras().getParcelable(Constants.TAG_PRODUCT_MODEL);
                    if (product.isAddedToWishList()) {
                        addWishListProduct(product);
                    } else {
                        removeExistingProduct(product);
                    }
                    updateCatalogProductList(product);
                    calculateWishListTotalCost();
                }
            }
        }
    }

    @SuppressLint("NewApi")
    private void calculateWishListTotalCost() {
        int totalPrice = 0;
        if (mWishListProducts.size() != 0) {
            List<Integer> priceList = new ArrayList<>();
            for (Product product : mWishListProducts) {
                int price = processPriceList(product.getPrice());
                priceList.add(price);
            }
            totalPrice = priceList.stream().mapToInt(a -> a).sum();
        }
        mTotal.setText("Total $" + totalPrice);
        mSubtotal.setText("$" + totalPrice);
    }

    private int processPriceList(String priceText) {
        priceText = priceText.replace("$ ", "");
        int price = Integer.parseInt(priceText);
        return price;
    }

    private void addWishListProduct(Product product) {
        boolean productExists = false;
        for (int i = 0; i < mWishListProducts.size(); i++) {
            if (mWishListProducts.get(i).getId() == product.getId()) {
                productExists = true;
                break;
            }
        }
        if (!productExists) {
            mWishListProducts.add(product);
            updateWishListAdapterData();
        }
    }

    private void removeExistingProduct(Product product) {
        for (int i = 0; i < mWishListProducts.size(); i++) {
            if (mWishListProducts.get(i).getId() == product.getId()) {
                mWishListProducts.remove(i);
                updateWishListAdapterData();
                break;
            }
        }
    }

    private void updateCatalogProductList(Product product) {
        for (int i = 0; i < mCatalogProductList.size(); i++) {
            if (mCatalogProductList.get(i).getId() == product.getId()) {
                mCatalogProductList.get(i).setAddedToWishList(product.isAddedToWishList());
                break;
            }
        }
        updateCatalogAdapterData();
    }

    @Override
    public void onWishItemClick(Product product) {
        startActivityForResult(ProductDetailsActivity.getMapsActivityIntent(this, product), Constants.REQUEST_CODE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.check_out:
                showCheckOutDialog();
                break;
        }
    }

    private void showCheckOutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.proceed_to_checkout)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    if (mWishListProducts.size() != 0) {
                        resetData();
                    }
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> {
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorBlue));
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorBlue));
        });
        alertDialog.show();
    }

    private void resetData() {
        resetCatalogProducts();
        resetWishListProducts();
        updateCatalogAdapterData();
        updateWishListAdapterData();
        calculateWishListTotalCost();
    }

    private void resetCatalogProducts() {
        for (Product product : mCatalogProductList) {
            product.setAddedToWishList(false);
        }
    }

    private void resetWishListProducts() {
        mWishListProducts.clear();
    }


}
