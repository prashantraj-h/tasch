package com.intact.tasch.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.intact.tasch.R;
import com.intact.tasch.model.Product;
import com.intact.tasch.utility.Constants;

public class ProductDetailsActivity extends BaseActivity {

    private TextView mToolbarTitle;
    private CheckBox mAddProductToWishList;
    private Product mProduct;
    private ImageView mProductImage;
    private TextView mPrice;
    private TextView mDescription;
    private TextView mDimensions;

    public static Intent getMapsActivityIntent(Activity activity, Product product) {
        Intent intent = new Intent(activity, ProductDetailsActivity.class);
        intent.putExtra(Constants.TAG_PRODUCT_MODEL, product);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        handleBundle(getIntent());
        initViews();
        setViewData();
        if (mProduct != null) {
            setCheckBoxStatus(mProduct.isAddedToWishList());
            setCheckBoxText(mProduct.isAddedToWishList());
        }
        setListeners();
    }

    private void handleBundle(Intent intent) {
        if (intent.getExtras() != null) {
            if (intent.hasExtra(Constants.TAG_PRODUCT_MODEL)) {
                mProduct = intent.getExtras().getParcelable(Constants.TAG_PRODUCT_MODEL);
            }
        }
    }

    private void initViews() {
        mToolbar = findViewById(R.id.toolbar_actionbar);
        mToolbarTitle = findViewById(R.id.toolbar_title);
        mAddProductToWishList = findViewById(R.id.wish_list_check_box);
        mProductImage = findViewById(R.id.product_detail_image);
        mPrice = findViewById(R.id.product_price);
        mDescription = findViewById(R.id.product_description);
        mDimensions = findViewById(R.id.dimensions);
    }

    private void setViewData() {
        mToolbarTitle.setText(mProduct.getName());
        int imageIndex = Integer.parseInt(mProduct.getImage());
        int image = Constants.iconList[imageIndex - 1];
        Glide.with(this)
                .load(image)
                .into(mProductImage);
        mPrice.setText(mProduct.getPrice());
        mDescription.setText(mProduct.getDescription());

        String height = "H:" + mProduct.getSize().get(0) + "\"";
        String width = "W: "+mProduct.getSize().get(1)+ "\"";
        String depth = "D: "+mProduct.getSize().get(2)+ "\"";

        mDimensions.setText(height + "\n" + width + "\n" + depth);

        for (String productColor : mProduct.getColors()) {
            addImage(productColor);
        }
    }

    private void setListeners() {
        mAddProductToWishList.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mProduct.setAddedToWishList(isChecked);
            setCheckBoxText(isChecked);
            setResult();
            onBackPressed();
        });
        mToolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void setResult() {
        Intent intent = new Intent();
        intent.putExtra(Constants.TAG_PRODUCT_MODEL, mProduct);
        setResult(Constants.REQUEST_CODE, intent);
    }

    private void addImage(String colorCode) {
        int imageColor = Color.parseColor(colorCode);
        ColorDrawable colorDrawable = new ColorDrawable(imageColor);
        ImageView imageView = new ImageView(this);
        LinearLayout parentLayout = findViewById(R.id.product_color_options);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200 ,200);
        imageView.setLayoutParams(layoutParams);
        Constants.setViewMargins(imageView, 0, 0, 40, 0);
        Glide.with(this)
                .load(colorDrawable)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners(15)))
                .into(imageView);

        parentLayout.addView(imageView);
    }

    private void setCheckBoxStatus(boolean addedToWishList) {
        mAddProductToWishList.setChecked(addedToWishList);
    }

    private void setCheckBoxText(boolean addedToWishList) {
        if (addedToWishList) {
            mAddProductToWishList.setText("Remove from wish list");
        } else {
            mAddProductToWishList.setText("Add to wish list");
        }
    }

}
