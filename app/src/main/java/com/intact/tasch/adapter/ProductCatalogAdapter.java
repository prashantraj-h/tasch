package com.intact.tasch.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.intact.tasch.R;
import com.intact.tasch.model.Product;
import com.intact.tasch.utility.Constants;

import java.util.List;

public class ProductCatalogAdapter extends RecyclerView.Adapter {

    private List<Product> mProductList;
    private Context mContext;
    private ClickListener mListener;

    public ProductCatalogAdapter(Context context, ClickListener listener) {
        mContext = context;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_catalog_item, viewGroup,false);
        RecyclerView.ViewHolder viewHolder = new ProductCatalogViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        final ProductCatalogViewHolder holder = (ProductCatalogViewHolder) viewHolder;
        Product product = getItem(position);

        String productName = product.getName();
        String image = product.getImage();
        int index = Integer.parseInt(image);
        int imageDrawable = Constants.iconList[index - 1];
        Glide.with(mContext)
                .load(imageDrawable)
                .into(holder.productCatalogImage);
        holder.productCatalogName.setText(productName);
        setListeners(holder, product);
    }

    private void setListeners(ProductCatalogViewHolder holder, final Product product) {
        holder.cardView.setOnClickListener(v -> mListener.onItemClick(product));
    }

    public Product getItem(int position) {
        return  mProductList.get(position);
    }

    @Override
    public int getItemCount() {
        if (mProductList != null) {
            return mProductList.size();
        }
        return 0;
    }

    public void setDataList(List<Product> productList) {
        mProductList = productList;
        notifyDataSetChanged();
    }

    static class ProductCatalogViewHolder extends  RecyclerView.ViewHolder {
        View cardView;
        ImageView productCatalogImage;
        TextView productCatalogName;

        public ProductCatalogViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.product_catalog_card);
            productCatalogImage = itemView.findViewById(R.id.product_catalog_image);
            productCatalogName = itemView.findViewById(R.id.product_catalog_name);
        }
    }

    public interface ClickListener {
        void onItemClick(Product product);
    }

}
