package com.intact.tasch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.intact.tasch.R;
import com.intact.tasch.model.Product;
import com.intact.tasch.utility.Constants;

import java.util.List;

public class WishListAdapter extends RecyclerView.Adapter {

    private List<Product> mProductList;
    private Context mContext;
    private WishListClickListener mListener;

    public WishListAdapter(Context context, WishListClickListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void setDataList(List<Product> productList) {
        mProductList = productList;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wish_list_item, viewGroup, false);
        RecyclerView.ViewHolder viewHolder = new WishListAdapter.WishListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        WishListViewHolder holder = (WishListViewHolder) viewHolder;
        Product product = getItem(position);
        String image = product.getImage();
        int index = Integer.parseInt(image);
        int imageDrawable = Constants.iconList[index - 1];
        Glide.with(mContext)
                .load(imageDrawable)
                .into(holder.productImage);

        holder.price.setText(product.getPrice());
        holder.productShortDescription.setText(product.getDescription());
        if (Integer.parseInt(product.getQuantity()) == 0) {
            holder.productOutOfStock.setVisibility(View.VISIBLE);
            holder.productColorOptions.setVisibility(View.INVISIBLE);
        } else {
            holder.productColorOptions.setVisibility(View.VISIBLE);
            holder.productOutOfStock.setVisibility(View.GONE);
            removeAllViews(holder);
            for (String color : product.getColors()) {
                addImage(holder, color);
            }
        }
        setListeners(holder, product);
    }

    private void addImage(WishListViewHolder holder, String colorCode) {
        int imageColor = Color.parseColor(colorCode);
        ColorDrawable colorDrawable = new ColorDrawable(imageColor);
        ImageView imageView = new ImageView(mContext);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(50, 50);
        imageView.setLayoutParams(layoutParams);
        Constants.setViewMargins(imageView, 0, 0, 5, 0);
        Glide.with(mContext)
                .load(colorDrawable)
                .apply(RequestOptions.bitmapTransform(new RoundedCorners(1)))
                .into(imageView);

        holder.productColorOptions.addView(imageView);
    }


    private void removeAllViews(WishListViewHolder holder) {
        holder.productColorOptions.removeAllViews();
    }

    private void setListeners(WishListViewHolder holder, Product product) {
        holder.cardView.setOnClickListener(v -> mListener.onWishItemClick(product));
    }

    public Product getItem(int position) {
        return mProductList.get(position);
    }

    @Override
    public int getItemCount() {
        if (mProductList != null) {
            return mProductList.size();
        }
        return 0;
    }

    static class WishListViewHolder extends RecyclerView.ViewHolder {
        View cardView;
        ImageView productImage;
        TextView price;
        TextView productType;
        TextView productShortDescription;
        LinearLayout productColorOptions;
        TextView productOutOfStock;


        public WishListViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.wish_list_card);
            productImage = itemView.findViewById(R.id.product_image);
            price = itemView.findViewById(R.id.price);
            productType = itemView.findViewById(R.id.bag_type);
            productShortDescription = itemView.findViewById(R.id.bag_description);
            productColorOptions = itemView.findViewById(R.id.product_color_options);
            productOutOfStock = itemView.findViewById(R.id.out_of_stock);
        }
    }

    public interface WishListClickListener {
        void onWishItemClick(Product product);
    }

}
