package com.intact.tasch.client;

import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.intact.tasch.model.DataModel;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.Modifier;

public class OfflineDataHandler {

    private ResponseListener mListener;

    public void fetchAssetsData(DataRequest request, ResponseListener listener) {
        mListener = listener;

        try {
            InputStream inputStream = request.getContext().getAssets().open(request.getFilePath());
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String productsJson = new String(buffer);
            new ParserTask(request).execute(productsJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ParserTask extends AsyncTask<String, Void, DataModel> {
        private DataRequest mRequest;

        ParserTask(DataRequest request) {
            mRequest = request;
        }

        @Override
        protected DataModel doInBackground(String... params) {
            DataModel model = null;
            try {
                Gson gson = new GsonBuilder().
                        excludeFieldsWithModifiers(Modifier.FINAL,
                                Modifier.TRANSIENT, Modifier.STATIC).create();
                JsonReader reader = new JsonReader(new StringReader(params[0].toString()));
                reader.setLenient(true);
                model = gson.fromJson(reader, mRequest.getDataModel().getClass());
            } catch (JsonSyntaxException exception) {
                Log.d("TAG_FAILURE", ""+exception.getMessage());
            }
            return model;
        }

        @Override
        protected void onPostExecute(DataModel dataModel) {
            if (dataModel != null) {
                mRequest.setDataModel(dataModel);
                mListener.onResponse();
            }
        }
    }

    public interface ResponseListener {
        void onResponse();
    }

}
