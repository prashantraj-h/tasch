package com.intact.tasch.client;

import android.content.Context;

import com.intact.tasch.model.DataModel;

public class DataRequest {

    private String mFilePath;
    private DataModel mDataModel;
    private Context mContext;

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        this.mFilePath = filePath;
    }

    public DataModel getDataModel() {
        return mDataModel;
    }

    public void setDataModel(DataModel dataModel) {
        this.mDataModel = dataModel;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }
}
