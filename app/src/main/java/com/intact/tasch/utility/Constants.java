package com.intact.tasch.utility;

import android.view.View;
import android.view.ViewGroup;

import com.intact.tasch.R;

public class Constants {

    public static final String FILE_PATH = "products.json";
    public static final String TAG_PRODUCT_MODEL = "product_model_tag";
    public static final int REQUEST_CODE = 1001;

    public static final int[] iconList = {R.drawable.bag_1, R.drawable.bag_2, R.drawable.bag_3,
            R.drawable.bag_4, R.drawable.bag_5};

    public static void setViewMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            marginLayoutParams.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }


}
