package com.intact.tasch.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataModel implements Parcelable {

    public DataModel() {
        //empty constructor required
    }

    @SerializedName("products")
    private List<Product> mProduct;

    protected DataModel(Parcel in) {
    }

    public static final Creator<DataModel> CREATOR = new Creator<DataModel>() {
        @Override
        public DataModel createFromParcel(Parcel in) {
            return new DataModel(in);
        }

        @Override
        public DataModel[] newArray(int size) {
            return new DataModel[size];
        }
    };

    public List<Product> getProduct() {
        return mProduct;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mProduct);
    }

}
