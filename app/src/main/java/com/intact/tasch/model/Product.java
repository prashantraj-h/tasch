package com.intact.tasch.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product implements Parcelable {

    public Product() {
        //empty constructor required
    }

    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("image")
    private String mImage;
    @SerializedName("size")
    private List <String> mSize;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("quantity")
    private  String mQuantity;
    @SerializedName("colors")
    private List <String> mColors;

    private boolean mAddedToWishList;

    protected Product(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mDescription = in.readString();
        mImage = in.readString();
        mSize = in.createStringArrayList();
        mPrice = in.readString();
        mQuantity = in.readString();
        mColors = in.createStringArrayList();
        mAddedToWishList = in.readByte() != 0;

    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getImage() {
        return mImage;
    }

    public String getPrice() {
        return mPrice;
    }

    public boolean isAddedToWishList() {
        return mAddedToWishList;
    }

    public List <String> getSize() {
        return mSize;
    }

    public void setAddedToWishList(boolean addedToWishList) {
        this.mAddedToWishList = addedToWishList;
    }

    public List<String> getColors() {
        return mColors;
    }

    public String getQuantity() {
        return mQuantity;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeString(mImage);
        dest.writeStringList(mSize);
        dest.writeString(mPrice);
        dest.writeString(mQuantity);
        dest.writeStringList(mColors);
        dest.writeByte((byte) (mAddedToWishList ? 1 : 0));
    }
}
